# Copyright 2012 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require pagure \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="GObject based library API for managing information about operating systems"
HOMEPAGE+=" https://${PN}.org"

LICENCES="GPL-2 LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
    ( linguas: af am anp ar as ast bal be bg bn_IN bn bo br brx bs ca cs cy da de_CH de el en_GB eo
               es et eu fa fi fr gl gu he hi hr hu ia id ilo is it ja ka kk km mn ko kw_GB kw
               kw@kkcor kw@uccor ky lt lv mai mk ml mn mr ms nb nds ne nl nn nso or pa pl pt_BR pt
               ro ru si sk sl sq sr sr@latin sv ta te tg th tr tw uk ur vi wba yo zh_CN zh_HK zh_TW
               zu )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-util/intltool[>=0.40.0]
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
        vapi? ( dev-lang/vala:* )
    build+run:
        dev-libs/glib:2[>=2.36]
        dev-libs/libxml2:2.0[>=2.6.0]
        dev-libs/libxslt[>=1.0.0]
        gnome-desktop/gobject-introspection:1[>=0.9.7] [[ note = [ required for running autotools ] ]]
        gnome-desktop/libsoup:2.4[>=2.42]
    run:
        sys-apps/osinfo-db-tools
    test:
        dev-libs/check
    test+run:
        sys-apps/osinfo-db
        sys-apps/pciutils-data
        sys-apps/usbutils-data
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-do-not-hardcode-ld.patch
    "${FILES}"/0002-do-not-hardcode-nm.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-static
    --with-pci-ids-path=/usr/share/misc/pci.ids
    --with-usb-ids-path=/usr/share/misc/usb.ids
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "gobject-introspection introspection"
    gtk-doc
    "vapi vala"
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-tests --disable-tests'
)

